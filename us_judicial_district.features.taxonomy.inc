<?php
/**
 * @file
 * us_judicial_district.features.taxonomy.inc
 */

/**
 * Implements hook_taxonomy_default_vocabularies().
 */
function us_judicial_district_taxonomy_default_vocabularies() {
  return array(
    'us_judicial_district' => array(
      'name' => 'U.S. Judicial District',
      'machine_name' => 'us_judicial_district',
      'description' => 'The 94 federal judicial districts in the United States.',
      'hierarchy' => 0,
      'module' => 'taxonomy',
      'weight' => 0,
      'rdf_mapping' => array(
        'rdftype' => array(
          0 => 'skos:ConceptScheme',
        ),
        'name' => array(
          'predicates' => array(
            0 => 'dc:title',
          ),
        ),
        'description' => array(
          'predicates' => array(
            0 => 'rdfs:comment',
          ),
        ),
      ),
    ),
  );
}
