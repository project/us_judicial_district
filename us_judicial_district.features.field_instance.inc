<?php
/**
 * @file
 * us_judicial_district.features.field_instance.inc
 */

/**
 * Implements hook_field_default_field_instances().
 */
function us_judicial_district_field_default_field_instances() {
  $field_instances = array();

  // Exported field_instance:
  // 'taxonomy_term-us_judicial_district-field_us_judicial_district_cite'.
  $field_instances['taxonomy_term-us_judicial_district-field_us_judicial_district_cite'] = array(
    'bundle' => 'us_judicial_district',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The abbreviation used for the purposes of case citations.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 3,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_us_judicial_district_cite',
    'label' => 'Abbreviation for case citations',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 32,
      ),
      'type' => 'text_textfield',
      'weight' => 2,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-us_judicial_district-field_us_judicial_district_geo'.
  $field_instances['taxonomy_term-us_judicial_district-field_us_judicial_district_geo'] = array(
    'bundle' => 'us_judicial_district',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The GeoJSON data describing the boundaries of this district.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 2,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_us_judicial_district_geo',
    'label' => 'Geographic region',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'geofield',
      'settings' => array(),
      'type' => 'geofield_geojson',
      'weight' => 4,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-us_judicial_district-field_us_judicial_district_sort'.
  $field_instances['taxonomy_term-us_judicial_district-field_us_judicial_district_sort'] = array(
    'bundle' => 'us_judicial_district',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => 'The abbreviation used for the purposes of sorting.',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'module' => 'text',
        'settings' => array(),
        'type' => 'text_default',
        'weight' => 4,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_us_judicial_district_sort',
    'label' => 'Abbreviation for sorting',
    'required' => 1,
    'settings' => array(
      'text_processing' => 0,
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'text',
      'settings' => array(
        'size' => 4,
      ),
      'type' => 'text_textfield',
      'weight' => 3,
    ),
  );

  // Exported field_instance:
  // 'taxonomy_term-us_judicial_district-field_us_judicial_district_state'.
  $field_instances['taxonomy_term-us_judicial_district-field_us_judicial_district_state'] = array(
    'bundle' => 'us_judicial_district',
    'default_value' => NULL,
    'deleted' => 0,
    'description' => '',
    'display' => array(
      'default' => array(
        'label' => 'above',
        'settings' => array(),
        'type' => 'hidden',
        'weight' => 1,
      ),
    ),
    'entity_type' => 'taxonomy_term',
    'field_name' => 'field_us_judicial_district_state',
    'label' => 'State',
    'required' => 1,
    'settings' => array(
      'user_register_form' => FALSE,
    ),
    'widget' => array(
      'active' => 1,
      'module' => 'options',
      'settings' => array(),
      'type' => 'options_select',
      'weight' => 5,
    ),
  );

  // Translatables
  // Included for use with string extractors like potx.
  t('Abbreviation for case citations');
  t('Abbreviation for sorting');
  t('Geographic region');
  t('State');
  t('The GeoJSON data describing the boundaries of this district.');
  t('The abbreviation used for the purposes of case citations.');
  t('The abbreviation used for the purposes of sorting.');

  return $field_instances;
}
