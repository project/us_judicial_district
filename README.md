# US Judicial District

## Overview

This is a Features module that sets up a taxonomy vocabulary for U.S. judicial districts. Each term gets some abbreviation data and geographical region data, via the Geofield module.

## Use case and related module

A possible use case is to associate an entity type with a district, so that users can search for the district that contains their location, or for entities that are within the same district as their location. To accomplish this, the [Geofield Filter Contains](https://www.drupal.org/project/geofield_filter_contains) module may be helpful.

## Installation

Enable the module as normal. The vocabulary and the terms will be automatically created.

## After installation

Because this module doesn't include any functionality, you are welcome to disable and uninstall the module immediately. The vocabulary and terms will remain. This will allow you to add the vocabulary to your own site-specific Features module, if desired.
