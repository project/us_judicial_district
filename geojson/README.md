# Generating the GeoJSON for the U.S. Judicial Districts

Rather than version all the source data, I will only version the final district files, and detail the steps I took to get them. Anyone with some basic GIS skills can surely improve on this process, but here is what I did:

* Download a [very large file](https://raw.githubusercontent.com/cbupp/judicial-boundaries/master/us-district-courts.geojson) from [this repo](https://github.com/cbupp/judicial-boundaries).
* Install some node tools:
    * npm i -g minify-geojson
    * npm i -g geojsplit
* Reduce the precision of the data:
    * minify-geojson -c 5 us-district-courts.geojson
* Split the data into separate files:
    * geojsplit -l 1 -o districts/ -k JD_NAME -a 0 us-district-courts.min.geojson
* Clean up:
    * rm *.geojson

Notes/questions:
* Is the minifying maybe not necessary? Seemed like it might help performance.
* Note sure how to avoid the "_x" in the filenames
